import React, { Component } from "react";
import "./App.css";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      APIURL: "",
      imageURL: "",
      title: "",
      date: "",
      description: "",
      video: false,
    };
  }

  componentDidMount() {
    this.setImage();
  }

  setImage = async () => {
    let result = 0;
    do {
      result = await this.getImageURL(
        Math.floor(Math.random() * 25 + 1995),
        Math.floor(Math.random() * 12 + 1),
        Math.floor(Math.random() * 31 + 1)
      );
      console.log("result is: ", result);
    } while (result === 0);
  };

  getImageURL = async (year, month, day) => {
    let URL =
      "https://api.nasa.gov/planetary/apod?api_key=FZRLeaKZfBmxbUHI9VWQPzKxAbRMCye58zW9oNLc&date=" +
      year +
      "-" +
      month +
      "-" +
      day;

    let blob = await fetch(URL);
    let resp = await blob.json();

    this.setState({
      APIURL: URL,
      imageURL: resp.hdurl === "undefined" ? resp.url : resp.hdurl,
      title: resp.title,
      date: resp.date,
      description: resp.explanation,
      video: resp.media_type === "video" ? true : false,
    });

    return resp.title === undefined ? 0 : 1;
  };

  render() {
    return (
      <div
        style={{
          backgroundImage: 'url("' + this.state.imageURL + '")',
          backgroundPosition: "center",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
          height: "100vh",
        }}
      >
        <span className={"data"}>
          <span>
            {this.state.title === undefined
              ? ""
              : this.state.title.toUpperCase()}
          </span>
          <br />
          {this.state.date + " - " + this.state.description}
        </span>
        <a
          className={"link"}
          href={this.state.APIURL}
          target="_blank"
          rel="noopener noreferrer"
        >
          {this.state.APIURL}
        </a>
        {this.state.video === true ? (
          <span className={"infoVideo"}>
            This is a video, open the link at the bottom to watch it!
          </span>
        ) : null}
        <button className={"nextButton"} onClick={() => this.setImage()}>
          NEXT
        </button>
      </div>
    );
  }
}
